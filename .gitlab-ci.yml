image: docker:19.03.10

services:
  - docker:dind

before_script:
    - apk add --no-cache curl unzip
    - curl -LO https://releases.hashicorp.com/terraform/1.0.0/terraform_1.0.0_linux_amd64.zip
    - unzip terraform_1.0.0_linux_amd64.zip
    - mv terraform /usr/local/bin/
    - terraform --version

stages:
  - terraform_validate
  - terraform_plan
  - terraform_deploy
  - python_bandit_scan
  - docker_build_deploy
  - trivy_scan
  - eks_deploy_sit
  - eks_deploy
  - environment_cleanup_sit
  - environment_cleanup

terraform_validate:
  stage: terraform_validate
  script:
    - echo "Validating Terraform..."
    - cd iac/
    - terraform init
    - terraform validate
  only:
    - sit
    - release
    - main
  needs: []

terraform_plan:
  stage: terraform_plan
  script:
    - echo "Planning Terraform..."
    - cd iac/
    - terraform init
    - terraform plan -out=tfplan
  only:
    - sit
    - release
    - main
  needs: ["terraform_validate"]

# terraform_deploy_sit:
#   stage: terraform_deploy_sit
#   script:
#     - echo "Deploying Terraform for sit..."
#     - cd iac/
#     - terraform init
#     - terraform apply -auto-approve
#   only:
#     - sit
    
#   needs: ["terraform_plan"]

terraform_deploy:
  stage: terraform_deploy
  script:
    - echo "Deploying Terraform for Release/Main..."
    - cd iac/
    - terraform init
    - terraform apply -auto-approve
  # when: manual
  only:
    - sit
    - main
    - release
  needs: ["terraform_plan"]

python_bandit_scan:
  before_script:
    - apk add --no-cache python3
    - pip3 install bandit

  stage: python_bandit_scan
  script:
    - echo "Running bandit scan..."
    - bandit -r . -f json | tee bandit_scan.json || true
  only:
    - sit
    - release
    - main
  needs:
    - job: terraform_deploy_sit
      optional: true
    - job: terraform_deploy
      optional: true

docker_build_deploy:
  before_script:
    - apk add --no-cache python3
    - pip3 install awscli 
    - aws configure set aws_access_key_id $AWS_ACCESS_KEY_ID
    - aws configure set aws_secret_access_key $AWS_SECRET_ACCESS_KEY
    - aws configure set region $AWS_DEFAULT_REGION
    - repositoryUri=$(aws ecr describe-repositories --region $AWS_DEFAULT_REGION --query 'repositories[?starts_with(@.repositoryName, `berkeley`) == `true`].repositoryUri' --output text)
    - IMAGE_TAG="$(echo $CI_COMMIT_SHA | head -c 8)"
    - aws ecr get-login-password --region $AWS_DEFAULT_REGION  | docker login --username AWS --password-stdin $repositoryUri

  stage: docker_build_deploy
  script:
    - echo "Building and Deploying Docker Image..."
    - docker build -t $repositoryUri:latest app/.
    - docker tag $repositoryUri:latest $repositoryUri:$IMAGE_TAG
    - docker push $repositoryUri:$IMAGE_TAG
  only:
    - sit
    - release
    - main
  needs: ["python_bandit_scan"]

trivy_scan:
  before_script:
    - apk add --no-cache python3 curl
    - pip3 install awscli 
    - aws configure set aws_access_key_id $AWS_ACCESS_KEY_ID
    - aws configure set aws_secret_access_key $AWS_SECRET_ACCESS_KEY
    - aws configure set region $AWS_DEFAULT_REGION
    - repositoryUri=$(aws ecr describe-repositories --region $AWS_DEFAULT_REGION --query 'repositories[?starts_with(@.repositoryName, `berkeley`) == `true`].repositoryUri' --output text)
    - IMAGE_TAG="$(echo $CI_COMMIT_SHA | head -c 8)"
    - curl -sfL https://raw.githubusercontent.com/aquasecurity/trivy/main/contrib/install.sh | sh -s -- -b /usr/local/bin

  stage: trivy_scan
  script:
    - echo "Running trivy scan..."
    - trivy image --format=json $repositoryUri:$IMAGE_TAG | tee trivy_scan.json
  only:
    - sit
    - release
    - main
  needs: ["docker_build_deploy"]

eks_deploy_sit:
  before_script:
    - apk add --no-cache curl python3
    - pip3 install awscli 
    - aws configure set aws_access_key_id $AWS_ACCESS_KEY_ID
    - aws configure set aws_secret_access_key $AWS_SECRET_ACCESS_KEY
    - aws configure set region $AWS_DEFAULT_REGION
    - security_group_id=$(aws ec2 describe-security-groups --query 'SecurityGroups[?starts_with(GroupName, `eks-cluster-sg-berkeley-`)].GroupId' --output text)
    - aws ec2 revoke-security-group-ingress --group-id $security_group_id  --ip-permissions   "`aws ec2 describe-security-groups --output json --group-ids $security_group_id --query "SecurityGroups[0].IpPermissions"`"
    - port="30080"; cidr="0.0.0.0/0"; aws ec2 authorize-security-group-ingress --group-id $security_group_id --protocol tcp --port $port --cidr $cidr
    - aws ec2 authorize-security-group-ingress --group-id $security_group_id   --source-group $security_group_id --protocol -1
    - repositoryUri=$(aws ecr describe-repositories --region $AWS_DEFAULT_REGION --query 'repositories[?starts_with(@.repositoryName, `berkeley`) == `true`].repositoryUri' --output text)
    - IMAGE_TAG="$(echo $CI_COMMIT_SHA | head -c 8)"
    - aws ecr get-login-password --region $AWS_DEFAULT_REGION  | docker login --username AWS --password-stdin $repositoryUri
    - curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl"
    - chmod +x ./kubectl
    - mv ./kubectl /usr/local/bin/kubectl

  stage: eks_deploy_sit
  script:
    - echo "Deploying to EKS for sit..."
    - aws eks --region $AWS_DEFAULT_REGION update-kubeconfig --name berkeley-eks-cluster 
    - kubectl delete deployment redis --ignore-not-found
    - kubectl create deployment redis --image=redis:alpine --port=6379 --replicas=1
    - kubectl delete service redis --ignore-not-found
    - kubectl create service clusterip redis --tcp=6379:6379
    - kubectl delete deployment berkeley-visitor-counter --ignore-not-found
    - kubectl create deployment berkeley-visitor-counter --image=$repositoryUri:$IMAGE_TAG --port=3000 --replicas=2 
    - kubectl create service nodeport berkeley-visitor-counter --tcp=3000:3000 --node-port=30080 --dry-run=client -o yaml > service.yaml
    - kubectl apply -f service.yaml
    - echo "The application is available at below IPs:"
    - aws ec2 describe-instances --filters "Name=tag:eks:nodegroup-name,Values=worker-group-*" --query "Reservations[*].Instances[*].[PublicIpAddress]"   --output text | awk '{print $0":30080"}'
  only:
    - sit
  needs: ["trivy_scan"]

eks_deploy_uat:
  before_script:
    - apk add --no-cache curl python3
    - pip3 install awscli 
    - aws configure set aws_access_key_id $AWS_ACCESS_KEY_ID
    - aws configure set aws_secret_access_key $AWS_SECRET_ACCESS_KEY
    - aws configure set region $AWS_DEFAULT_REGION
    - security_group_id=$(aws ec2 describe-security-groups --query 'SecurityGroups[?starts_with(GroupName, `eks-cluster-sg-berkeley-`)].GroupId' --output text)
    - aws ec2 revoke-security-group-ingress --group-id $security_group_id  --ip-permissions   "`aws ec2 describe-security-groups --output json --group-ids $security_group_id --query "SecurityGroups[0].IpPermissions"`"
    - port="30080"; cidr="0.0.0.0/0"; aws ec2 authorize-security-group-ingress --group-id $security_group_id --protocol tcp --port $port --cidr $cidr
    - aws ec2 authorize-security-group-ingress --group-id $security_group_id   --source-group $security_group_id --protocol -1
    - repositoryUri=$(aws ecr describe-repositories --region $AWS_DEFAULT_REGION --query 'repositories[?starts_with(@.repositoryName, `berkeley`) == `true`].repositoryUri' --output text)
    - IMAGE_TAG="$(echo $CI_COMMIT_SHA | head -c 8)"
    - aws ecr get-login-password --region $AWS_DEFAULT_REGION  | docker login --username AWS --password-stdin $repositoryUri
    - curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl"
    - chmod +x ./kubectl
    - mv ./kubectl /usr/local/bin/kubectl

  stage: eks_deploy
  script:
    - echo "Deploying to EKS for Release/Main..."
    - aws eks --region $AWS_DEFAULT_REGION update-kubeconfig --name berkeley-eks-cluster 
    - kubectl delete deployment redis --ignore-not-found
    - kubectl create deployment redis --image=redis:alpine --port=6379 --replicas=1
    - kubectl delete service redis --ignore-not-found
    - kubectl create service clusterip redis --tcp=6379:6379
    - kubectl delete deployment berkeley-visitor-counter --ignore-not-found
    - kubectl create deployment berkeley-visitor-counter --image=$repositoryUri:$IMAGE_TAG --port=3000 --replicas=2 
    - kubectl create service nodeport berkeley-visitor-counter --tcp=3000:3000 --node-port=30080 --dry-run=client -o yaml > service.yaml
    - kubectl apply -f service.yaml
    - echo "The application is available at below IPs:"
    - aws ec2 describe-instances --filters "Name=tag:eks:nodegroup-name,Values=worker-group-*" --query "Reservations[*].Instances[*].[PublicIpAddress]"   --output text | awk '{print $0":30080"}'
  when: manual
  only:
    - main
    - release
  needs: ["trivy_scan"]


eks_deploy_prod:
  before_script:
    - apk add --no-cache curl python3
    - pip3 install awscli 
    - aws configure set aws_access_key_id $AWS_ACCESS_KEY_ID
    - aws configure set aws_secret_access_key $AWS_SECRET_ACCESS_KEY
    - aws configure set region $AWS_DEFAULT_REGION
    - security_group_id=$(aws ec2 describe-security-groups --query 'SecurityGroups[?starts_with(GroupName, `eks-cluster-sg-berkeley-`)].GroupId' --output text)
    - aws ec2 revoke-security-group-ingress --group-id $security_group_id  --ip-permissions   "`aws ec2 describe-security-groups --output json --group-ids $security_group_id --query "SecurityGroups[0].IpPermissions"`"
    - port="30080"; cidr="0.0.0.0/0"; aws ec2 authorize-security-group-ingress --group-id $security_group_id --protocol tcp --port $port --cidr $cidr
    - aws ec2 authorize-security-group-ingress --group-id $security_group_id   --source-group $security_group_id --protocol -1
    - repositoryUri=$(aws ecr describe-repositories --region $AWS_DEFAULT_REGION --query 'repositories[?starts_with(@.repositoryName, `berkeley`) == `true`].repositoryUri' --output text)
    - IMAGE_TAG="$(echo $CI_COMMIT_SHA | head -c 8)"
    - aws ecr get-login-password --region $AWS_DEFAULT_REGION  | docker login --username AWS --password-stdin $repositoryUri
    - curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl"
    - chmod +x ./kubectl
    - mv ./kubectl /usr/local/bin/kubectl

  stage: eks_deploy
  script:
    - echo "Deploying to EKS for Release/Main..."
    - aws eks --region $AWS_DEFAULT_REGION update-kubeconfig --name berkeley-eks-cluster 
    - kubectl delete deployment redis --ignore-not-found
    - kubectl create deployment redis --image=redis:alpine --port=6379 --replicas=1
    - kubectl delete service redis --ignore-not-found
    - kubectl create service clusterip redis --tcp=6379:6379
    - kubectl delete deployment berkeley-visitor-counter --ignore-not-found
    - kubectl create deployment berkeley-visitor-counter --image=$repositoryUri:$IMAGE_TAG --port=3000 --replicas=2 
    - kubectl create service nodeport berkeley-visitor-counter --tcp=3000:3000 --node-port=30080 --dry-run=client -o yaml > service.yaml
    - kubectl apply -f service.yaml
    - echo "The application is available at below IPs:"
    - aws ec2 describe-instances --filters "Name=tag:eks:nodegroup-name,Values=worker-group-*" --query "Reservations[*].Instances[*].[PublicIpAddress]"   --output text | awk '{print $0":30080"}'
  when: manual
  only:
    - main
    - release
  needs: ["trivy_scan"]

environment_cleanup_sit:
  stage: environment_cleanup_sit
  script:
    - echo "Cleaning up environment for sit..."
    - cd iac/
    - terraform init
    - terraform destroy -auto-approve
  only:
    - sit
  needs: ["eks_deploy_sit"]

environment_cleanup:
  stage: environment_cleanup
  script:
    - echo "Cleaning up environment for Release/Main..."
    - cd iac/
    - terraform init
    - terraform destroy -auto-approve
  when: manual
  only:
    - release
    - main
  needs: ["eks_deploy_uat"]
