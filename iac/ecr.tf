resource "aws_ecr_repository" "newrepo" {
  name                 = "berkeley_repo-${random_string.suffix.result}"
  image_tag_mutability = "IMMUTABLE"

  image_scanning_configuration {
    scan_on_push = false
  }
  tags = {
    "Name" = "berkeley-repo"
  }
  force_delete = true
}

