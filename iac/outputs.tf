output "endpoint" {
  value = aws_eks_cluster.berkeley-eks.endpoint
}


output "Berkeley-EKS-Cluster-Name" {
  value = aws_eks_cluster.berkeley-eks.name
}

output "Berkeley-EKS-WorkerNode-Role-Name" {
  value = aws_iam_role.berkeley-eks-wn-role.name
}

# output "Berkeley-EKS-s3-ssrf-BucketName" {
#   value = aws_s3_bucket.ssrf_bucket.bucket
# }
